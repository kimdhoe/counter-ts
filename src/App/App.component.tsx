import * as React from 'react'

import './App.css'

interface Props {
  count: number
  onIncrement: () => void
  onDecrement: () => void
}

const App = ({ count, onIncrement, onDecrement }: Props)  => (
  <div className="App">
    <div className="App-header">
      <h2>Counter</h2>
    </div>
    <p className="App-intro">
      {count}
    </p>
    <div>
      <button onClick={onIncrement}>+</button>
      <button onClick={onDecrement}>-</button>
    </div>
  </div>
)

export default App
export { Props }
