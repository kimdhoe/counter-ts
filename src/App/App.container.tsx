import { connect, Dispatch } from 'react-redux'

import { StoreState } from '../world/type'
import * as action from '../world/action'
import AppComponent from './App.component'

const mapStateToProps = ({ count }: StoreState) => ({
  count
})

const mapDispatchToProps = (dispatch: Dispatch<action.CounterAction>) => ({
  onIncrement: () => dispatch(action.increase()),
    onDecrement: () => dispatch(action.decrease()),
})

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(AppComponent)

export default AppContainer
