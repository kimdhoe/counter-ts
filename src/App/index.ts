import AppComponent from './App.component'
import AppContainer from './App.container'

export {
  AppComponent,
  AppContainer,
}
