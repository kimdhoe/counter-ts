import * as constants from '../constants'

interface Increase {
  type: constants.INC
}

interface Decrease {
  type: constants.DEC
}

type CounterAction = Increase | Decrease

const increase = (): Increase => ({
  type: constants.INC
})

const decrease = (): Decrease => ({
  type: constants.DEC
})

export {
  Increase,
  Decrease,
  CounterAction,
  increase,
  decrease
}
