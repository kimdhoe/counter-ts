export const INC = 'INC'
export type INC = typeof INC

export const DEC = 'DEC'
export type DEC = typeof DEC
