import { CounterAction } from '../action'
import * as constants from '../constants'

const count = (state: number = 0, action: CounterAction) => {
  switch (action.type) {
    case constants.INC:
      return state + 1

    case constants.DEC:
      return state - 1

    default:
      return state
  }
}

export default count
