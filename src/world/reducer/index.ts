import { combineReducers, Reducer } from 'redux'

import count from './count'
import { StoreState } from '../type'

const reducer: Reducer<StoreState> = combineReducers({
  count,
})

export default reducer
